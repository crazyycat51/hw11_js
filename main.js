"use strict"

const input1 = document.querySelector("#inp1");
const input2 = document.querySelector("#inp2");
const picture1 = document.querySelector("#show");
const picture2 = document.querySelector("#show1");
const buttonSubmit = document.querySelector("#submit-button");

function toggleType(input, picture) {
    if (input.getAttribute('type') == 'password') {
        input.removeAttribute('type');
        input.setAttribute('type', 'text');
        picture.classList.remove('fa-eye');
        picture.classList.add('fa-eye-slash');
    } else {
        input.removeAttribute('type');
        input.setAttribute('type', 'password');
        picture.classList.remove('fa-eye-slash');
        picture.classList.add('fa-eye');
    }
}

picture1.onclick = function () {
    toggleType(input1, picture1);
}

picture2.onclick = function () {
    toggleType(input2, picture2);
}

buttonSubmit.onclick = function (event) {
    event.preventDefault();

    if (input1.value === input2.value) {
        alert("You are welcome");
    }
    else {
        alert("Введите одинаковые пароли");
    }
}